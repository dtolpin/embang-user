(ns embang-user.worksheet
  (:require [gorilla-plot.core :as plot])
  (:use [embang core emit runtime
         [state :only [get-predicts get-log-weight]]]))
