# __m!__ user project template

This repository provides template for a quick start
with [__m!__ Probabilistic Programming
Concept](https://bitbucket.org/dtolpin/embang).  The repository is
a [Leiningen](http://leiningen.org/) project. Probabilistic queries
can be run either in the Leiningen REPL, or inside the browser using
[Gorilla REPL](http://gorilla-repl.org/).

Checkout and modify for your needs. `programs` is intended for
standalone programs run in REPL or from the command line,
`worksheets` is for Gorilla REPL worksheets. Take a look a the
[Introduction to
__m!__](https://bitbucket.org/dtolpin/embang/src/HEAD/code/doc/intro.md)
if you are new to __m!__.  __m!__ is built on top of Clojure and
syntactically supports a subset of Clojure. This [Clojure tutorial](http://clojure-doc.org/articles/tutorials/introduction.html)
is a sufficient introduction to the Clojure language for __m!__
programmers.

The easiest way to start a new program is to copy
`programs/template.clj` (for a standalone program) or
`worksheets/template.clj` (for Gorilla worksheets) to a file with a
different name in the same directory and start modifying it.
