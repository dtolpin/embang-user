;; gorilla-repl.fileformat = 1

;; **
;;; Boiler-plate code --- importing necessary things.
;; **

;; @@
(use 'nstools.ns)
(ns+ flat
  (:like embang-user.worksheet))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"},{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}],"value":"[nil,nil]"}
;; <=

;; **
;;; Let us define a simple query.
;; **

;; @@
(defquery  flat
  (let [a (sample (uniform-discrete 0 10))
        c (sample (uniform-discrete (- 8 a) 10))]
    (observe (flip 1.) (= (+ a c) 13))
    (predict :a a)))

;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;flat/flat</span>","value":"#'flat/flat"}
;; <=

;; **
;;; Now, we lazily perform the inference.
;; **

;; @@
(def samples (doquery :pgibbs flat nil :number-of-particles 200))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;flat/samples</span>","value":"#'flat/samples"}
;; <=

;; **
;;; And retrieve predicts from the lazy sequence.
;; **

;; @@
(def N 10000)
(def as (map #(get % :a) (map get-predicts (take N (drop N samples)))))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;flat/N</span>","value":"#'flat/N"},{"type":"html","content":"<span class='clj-var'>#&#x27;flat/as</span>","value":"#'flat/as"}],"value":"[#'flat/N,#'flat/as]"}
;; <=

;; @@
(plot/histogram as)
;; @@
;; =>
;;; {"type":"vega","content":{"width":400,"height":247.2187957763672,"padding":{"top":10,"left":55,"bottom":40,"right":10},"data":[{"name":"fa431b6c-9ea0-40d5-a335-62b0cd48eddd","values":[{"x":0.0,"y":0},{"x":0.6000000000000001,"y":111.0},{"x":1.2000000000000002,"y":112.0},{"x":1.8000000000000003,"y":0.0},{"x":2.4000000000000004,"y":107.0},{"x":3.0000000000000004,"y":117.0},{"x":3.6000000000000005,"y":0.0},{"x":4.200000000000001,"y":2134.0},{"x":4.800000000000001,"y":0.0},{"x":5.4,"y":1955.0},{"x":6.0,"y":0.0},{"x":6.6,"y":1710.0},{"x":7.199999999999999,"y":1438.0},{"x":7.799999999999999,"y":0.0},{"x":8.399999999999999,"y":1260.0},{"x":8.999999999999998,"y":0.0},{"x":9.599999999999998,"y":1056.0},{"x":10.199999999999998,"y":0}]}],"marks":[{"type":"line","from":{"data":"fa431b6c-9ea0-40d5-a335-62b0cd48eddd"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":{"data":"fa431b6c-9ea0-40d5-a335-62b0cd48eddd","field":"data.x"}},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"fa431b6c-9ea0-40d5-a335-62b0cd48eddd","field":"data.y"}}],"axes":[{"type":"x","scale":"x"},{"type":"y","scale":"y"}]},"value":"#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :data [{:name \"fa431b6c-9ea0-40d5-a335-62b0cd48eddd\", :values ({:x 0.0, :y 0} {:x 0.6000000000000001, :y 111.0} {:x 1.2000000000000002, :y 112.0} {:x 1.8000000000000003, :y 0.0} {:x 2.4000000000000004, :y 107.0} {:x 3.0000000000000004, :y 117.0} {:x 3.6000000000000005, :y 0.0} {:x 4.200000000000001, :y 2134.0} {:x 4.800000000000001, :y 0.0} {:x 5.4, :y 1955.0} {:x 6.0, :y 0.0} {:x 6.6, :y 1710.0} {:x 7.199999999999999, :y 1438.0} {:x 7.799999999999999, :y 0.0} {:x 8.399999999999999, :y 1260.0} {:x 8.999999999999998, :y 0.0} {:x 9.599999999999998, :y 1056.0} {:x 10.199999999999998, :y 0})}], :marks [{:type \"line\", :from {:data \"fa431b6c-9ea0-40d5-a335-62b0cd48eddd\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"fa431b6c-9ea0-40d5-a335-62b0cd48eddd\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"fa431b6c-9ea0-40d5-a335-62b0cd48eddd\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}]}}"}
;; <=

;; **
;;; That's it.
;; **
