;; gorilla-repl.fileformat = 1

;; **
;;; Boiler-plate code --- importing necessary things.
;; **

;; @@
(use 'nstools.ns)
(ns+ coin-flip
  (:like embang-user.worksheet))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"},{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}],"value":"[nil,nil]"}
;; <=

;; **
;;; Let us define a simple query.
;; **

;; @@

(defquery coin-flip
  (let [bias (sample (beta 1.5 1.5))]
    (observe (flip bias) false)
    (observe (flip bias) false)
    (observe (flip bias) true)
    (observe (flip bias) false)
    (observe (flip bias) false)
    (observe (flip bias) true)
    (predict :bias bias)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;coin-flip/coin-flip</span>","value":"#'coin-flip/coin-flip"}
;; <=

;; **
;;; Now, we lazily perform the inference.
;; **

;; @@
(def samples (doquery :lmh coin-flip nil))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;coin-flip/samples</span>","value":"#'coin-flip/samples"}
;; <=

;; **
;;; And retrieve predicts from the lazy sequence.
;; **

;; @@
(def N 10000)
(def predicts (->> samples
                   (drop N)
                   (take N)
                   (map get-predicts)))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;coin-flip/N</span>","value":"#'coin-flip/N"},{"type":"html","content":"<span class='clj-var'>#&#x27;coin-flip/predicts</span>","value":"#'coin-flip/predicts"}],"value":"[#'coin-flip/N,#'coin-flip/predicts]"}
;; <=

;; **
;;; We can compute basic statistics, say the mean.
;;; 
;; **

;; @@
(def bias-mean (double (/ (reduce + (map :bias predicts)) (count predicts))))
bias-mean
                       
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;coin-flip/bias-mean</span>","value":"#'coin-flip/bias-mean"},{"type":"html","content":"<span class='clj-double'>0.39106105206129144</span>","value":"0.39106105206129144"}],"value":"[#'coin-flip/bias-mean,0.39106105206129144]"}
;; <=

;; **
;;; And even visualize the pdf!
;; **

;; @@
(plot/compose
  (plot/histogram (sort (map :bias predicts)) :bins 30 :plot-range [[0 1] :all])
  (plot/list-plot [[bias-mean 0] [bias-mean 1000]] :color "red" :joined true))
;; @@
;; =>
;;; {"type":"vega","content":{"width":400,"height":247.2187957763672,"padding":{"top":10,"left":55,"bottom":40,"right":10},"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":[0,1]},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"7ad31bb6-35a7-4ac1-8b4d-51a1e7b3d54f","field":"data.y"}}],"axes":[{"type":"x","scale":"x"},{"type":"y","scale":"y"}],"data":[{"name":"7ad31bb6-35a7-4ac1-8b4d-51a1e7b3d54f","values":[{"x":0.0,"y":0},{"x":0.03333333333333334,"y":3.0},{"x":0.06666666666666668,"y":53.0},{"x":0.10000000000000002,"y":113.0},{"x":0.13333333333333336,"y":202.0},{"x":0.16666666666666669,"y":335.0},{"x":0.2,"y":431.0},{"x":0.23333333333333334,"y":542.0},{"x":0.26666666666666666,"y":610.0},{"x":0.3,"y":714.0},{"x":0.3333333333333333,"y":764.0},{"x":0.36666666666666664,"y":828.0},{"x":0.39999999999999997,"y":766.0},{"x":0.4333333333333333,"y":832.0},{"x":0.4666666666666666,"y":692.0},{"x":0.49999999999999994,"y":619.0},{"x":0.5333333333333333,"y":619.0},{"x":0.5666666666666667,"y":470.0},{"x":0.6,"y":368.0},{"x":0.6333333333333333,"y":337.0},{"x":0.6666666666666666,"y":259.0},{"x":0.7,"y":169.0},{"x":0.7333333333333333,"y":111.0},{"x":0.7666666666666666,"y":67.0},{"x":0.7999999999999999,"y":47.0},{"x":0.8333333333333333,"y":28.0},{"x":0.8666666666666666,"y":16.0},{"x":0.8999999999999999,"y":4.0},{"x":0.9333333333333332,"y":0.0},{"x":0.9666666666666666,"y":1.0},{"x":0.9999999999999999,"y":0.0},{"x":1.0333333333333332,"y":0.0},{"x":1.0666666666666667,"y":0}]},{"name":"5a3f453b-aa1c-4efe-a67f-5975e4fd86fe","values":[{"x":0.39106105206129144,"y":0},{"x":0.39106105206129144,"y":1000}]}],"marks":[{"type":"line","from":{"data":"7ad31bb6-35a7-4ac1-8b4d-51a1e7b3d54f"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}},{"type":"line","from":{"data":"5a3f453b-aa1c-4efe-a67f-5975e4fd86fe"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"stroke":{"value":"red"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}]},"value":"#gorilla_repl.vega.VegaView{:content {:width 400, :height 247.2188, :padding {:top 10, :left 55, :bottom 40, :right 10}, :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain [0 1]} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"7ad31bb6-35a7-4ac1-8b4d-51a1e7b3d54f\", :field \"data.y\"}}], :axes [{:type \"x\", :scale \"x\"} {:type \"y\", :scale \"y\"}], :data ({:name \"7ad31bb6-35a7-4ac1-8b4d-51a1e7b3d54f\", :values ({:x 0.0, :y 0} {:x 0.03333333333333334, :y 3.0} {:x 0.06666666666666668, :y 53.0} {:x 0.10000000000000002, :y 113.0} {:x 0.13333333333333336, :y 202.0} {:x 0.16666666666666669, :y 335.0} {:x 0.2, :y 431.0} {:x 0.23333333333333334, :y 542.0} {:x 0.26666666666666666, :y 610.0} {:x 0.3, :y 714.0} {:x 0.3333333333333333, :y 764.0} {:x 0.36666666666666664, :y 828.0} {:x 0.39999999999999997, :y 766.0} {:x 0.4333333333333333, :y 832.0} {:x 0.4666666666666666, :y 692.0} {:x 0.49999999999999994, :y 619.0} {:x 0.5333333333333333, :y 619.0} {:x 0.5666666666666667, :y 470.0} {:x 0.6, :y 368.0} {:x 0.6333333333333333, :y 337.0} {:x 0.6666666666666666, :y 259.0} {:x 0.7, :y 169.0} {:x 0.7333333333333333, :y 111.0} {:x 0.7666666666666666, :y 67.0} {:x 0.7999999999999999, :y 47.0} {:x 0.8333333333333333, :y 28.0} {:x 0.8666666666666666, :y 16.0} {:x 0.8999999999999999, :y 4.0} {:x 0.9333333333333332, :y 0.0} {:x 0.9666666666666666, :y 1.0} {:x 0.9999999999999999, :y 0.0} {:x 1.0333333333333332, :y 0.0} {:x 1.0666666666666667, :y 0})} {:name \"5a3f453b-aa1c-4efe-a67f-5975e4fd86fe\", :values ({:x 0.39106105206129144, :y 0} {:x 0.39106105206129144, :y 1000})}), :marks ({:type \"line\", :from {:data \"7ad31bb6-35a7-4ac1-8b4d-51a1e7b3d54f\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}} {:type \"line\", :from {:data \"5a3f453b-aa1c-4efe-a67f-5975e4fd86fe\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :stroke {:value \"red\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}})}}"}
;; <=

;; @@

;; @@
